import * as TE from "fp-ts/TaskEither";
import { responses, successData } from "./main.spec";
import axios, { AxiosError, AxiosResponse } from "axios";
import { flow, pipe } from "fp-ts/lib/function";
import {
  ApiError,
  AuthError,
  RequestError,
  ServerError,
  UnexpectedBehavior,
} from "./errorTypes";

const makeGetRequest = TE.tryCatchK(
  (url: string) => axios.get(url.toString()),
  (reason): RequestError => {
    if (!(reason instanceof AxiosError && reason.response)) {
      return new UnexpectedBehavior();
    }

    const { status } = reason.response;

    switch (status) {
      case 500: {
        return new ServerError();
      }

      case 401: {
        return new AuthError();
      }

      case 404: {
        return new ApiError();
      }

      /* 418 error is handled as Unexpected */
      default: {
        return new UnexpectedBehavior();
      }
    }
  }
);

const getResponseData = TE.match<
  RequestError,
  typeof successData,
  AxiosResponse<typeof successData>
>(
  (err) => {
    throw err;
  },
  () => successData
);

const idURL = (n: keyof typeof responses) => `https://example.com/api/${n}`;

const idReqPipeline = flow(idURL, makeGetRequest, getResponseData);

export const getResponse = async (n: keyof typeof responses) => {
  return await pipe(n, idReqPipeline)();
};

export function getSomething(id: number): number {
  if (id === 1) {
    throw new Error("ERROR OMG");
  }
  return id;
}
