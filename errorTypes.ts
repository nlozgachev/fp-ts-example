interface BasicErrorParams {
  name: string;
  message: string;
}
class BasicError extends Error {
  constructor({ name, message }: BasicErrorParams) {
    super();
    this.name = name;
    this.message = message;
  }
}

export class ServerError extends BasicError {
  constructor() {
    super({ name: "ServerError", message: "Server is unreachable" });
  }
}

export class ApiError extends BasicError {
  constructor() {
    super({ name: "ApiError", message: "API signals error" });
  }
}

export class AuthError extends BasicError {
  constructor() {
    super({ name: "AuthError", message: "Auth failure" });
  }
}

export class UnexpectedBehavior extends BasicError {
  constructor() {
    super({ name: "UnexpectedBehavior", message: "Unexpected" });
  }
}

export type RequestError =
  | ServerError
  | ApiError
  | AuthError
  | UnexpectedBehavior;
