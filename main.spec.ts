import { afterAll, afterEach, beforeAll, describe, expect, it } from "vitest";
import { rest } from "msw";
import { setupServer } from "msw/node";
import { getResponse, getSomething } from "./main";
import {
  ApiError,
  AuthError,
  ServerError,
  UnexpectedBehavior,
} from "./errorTypes";

export const successData = { code: 200, message: "ok" };

export const responses = {
  200: successData,
  500: { code: 500 },
  401: { code: 401 },
  404: { code: 404 },
  418: { code: 418 },
} as const;

export const restHandlers = [
  rest.get("https://example.com/api/:id", (req, res, ctx) => {
    const { id } = req.params;
    const found = Object.keys(responses).find((r) => r === id);
    if (found) {
      return res(ctx.status(responses[found].code), ctx.json(responses[found]));
    }
    return res(ctx.status(responses[418].code), ctx.json(responses[418]));
  }),
];
const server = setupServer(...restHandlers);

beforeAll(() => server.listen({ onUnhandledRequest: "error" }));
afterAll(() => server.close());
afterEach(() => server.resetHandlers());

describe("requests", () => {
  it("should handle success", async () => {
    expect(await getResponse(200)).toEqual(responses[200]);
  });
  it("should handle ServerError", () => {
    expect(getResponse(500)).rejects.toThrowError(ServerError);
  });
  it("should handle AuthError", () => {
    expect(getResponse(401)).rejects.toThrowError(AuthError);
  });
  it("should handle ApiError", () => {
    expect(getResponse(404)).rejects.toThrowError(ApiError);
  });
  it("should handle UnexpectedBehavior", () => {
    expect(getResponse(418)).rejects.toThrowError(UnexpectedBehavior);
  });
});

describe("regular TS handling", () => {
  it.fails("throws error", () => {
    expect(getSomething(1)).toEqual(1);
  });
  it("ok", () => {
    expect(getSomething(2)).toEqual(2);
  });
});
